package enoxs.com.tutor.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class S1_HelloController {
    @RequestMapping(value = "/")
    public String home() {
        return "Spring Boot Sample Example Start Successful";
    }

    @RequestMapping("/hello")
    public String hello() {
        return "Hello World! Welcome to visit Spring Boot SE";
    }
}
